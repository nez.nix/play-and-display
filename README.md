# Play and Display

Updated version of this project:

https://github.com/djfun/audio-visualizer-python

Dependencies
------------
You need Python 3, PyQt5, PIL (or Pillow), and numpy.

You also need ffmpeg which grabs the audio stream from your source and renders the video.

Installation
------------
### Manual installation on Ubuntu
* Get all the python stuff: `sudo apt install python3 python3-pip python3-pyqt5 python3-pil python3-numpy ffmpeg`

* Install pillow, which I believe is necessary when using PyQt5: `pip3 install pillow`

You may have pillow already installed but run the command anyway to make sure.

Clone this repository and run the main program with `python3 main.py`.

